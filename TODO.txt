This is a personal tracker of features to be implemented in the next update. Pretty useless to anyone else.
Markers :
0 : To Do.
1 : Sprites missing.
2 : Implemented, needs crosslinking.
3 : Crosslinked, needs market value and crafting-ability.
4 : Only requires research attribution if applyable.
! : Done.

2 Hediffs

2	Oripathy sickness
		Affects whole body.
		Progress is shown with this factor :
			Blood Originium-Crystal Density : 0~0.15u/L : normal (starts at 0,10u/L). 0,16u/L : Start. 0.30u/L Mid-stage. 0.40u/L = Advanced. 0.52u/L = Death.
			(Other possible indicator : Cell-Originium Assimilation : Percentage. 0%8%~12% = Advanced. 20% = Insta-Death)
		Sickness progresses by 0.001u/L per day once infected.
		Possible addition : Starting from advanced, organ failure may occure randomly, crippling the affected pawn. Also, some body parts get affected (loss of hearing, loss of sight, movement disorder, etc.)

1 Mineables :

1	Originium crystals (small, medium, large).
		Make Originium crystals spread Oripathy.
		Mining crystals always loots originium shards and may loot Originite Prime.
		Mining crystals or being near them increases Oripathy.

Items :

2	Originite Prime
		Can either be mined or (mostly) traded.

2	Originium Shard
		Can be mined.

2	Orundum
		Refined Originium. Can be traded.

1	Orundum Catalyst
		Crafting element for Arts-based weaponry. Can be fabricated in a fabrication bench.

!	[DEBUG] Oripathy Clearer
		Removes oripathy upon ingestion
!	[DEBUG] Oripathy Giver
		Gives oripathy and adds 0.1 severity to it upon ingestion. (Lethal severity is 0.52)


Crafting spots :
0	Originium Weaponry Crafting Station
		Used to make weapons that use originium.

0	Orundum Refinery Station
		Used to make Orundum from originite prime and originite shards.

1 Weapons :
2 	Short Crossbow
1 	Heavy Crossbow

! Research :
!	Originium Processing
		Unlocks the Orundum Refinery Station and its subsequent crafts.
!	Originium Tech
		Unlocks the Originium Weaponry Crafting Station and its subsequent crafts.

Production :
0	Small Originium Power Plant
0		Consumes originium shards to generate electricity.